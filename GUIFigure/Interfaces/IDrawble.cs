﻿namespace GUIFigure.Interfaces
{
    internal interface IDrawble< TEntity >
    {
        void Draw( TEntity entity );
    }
}