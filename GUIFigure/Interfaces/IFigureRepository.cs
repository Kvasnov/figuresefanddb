﻿using System;
using System.Collections.Generic;
using GUIFigure.Classes;

namespace GUIFigure.Interfaces
{
    public interface IFigureRepository<TEntity> : IDisposable
    {
        event EventHandler<FigureAddedOrRemovedEventArgs<TEntity>> FigureAdded;
        event EventHandler<FigureAddedOrRemovedEventArgs<TEntity>> FigureRemoved;
        event EventHandler<FigureAddedOrRemovedEventArgs<TEntity>> FigureUpdated;

        IEnumerable<TEntity> GetFiguresList();
        TEntity GetFigure(Guid id);
        void Add(TEntity entity);
        void Remove(Guid id);
        void Update(TEntity entity);
    }
}
