﻿using System.Windows.Forms;
using Models;

namespace GUIFigure
{
    public partial class FormAddOrUpdateCircle : Form
    {
        public FormAddOrUpdateCircle( CircleModel model )
        {
            InitializeComponent();
            _model = model;
            textBoxRadius.DataBindings.Add( new Binding( nameof( TextBox.Text ), _model, nameof( CircleModel.Radius ) ) { DataSourceUpdateMode = DataSourceUpdateMode.OnPropertyChanged } );

            buttonOK.DataBindings.Add( new Binding( nameof( Enabled ), _model, nameof( CircleModel.IsValid ) ) );
        }

        private readonly CircleModel _model;

        private void textBoxRadius_KeyPress( object sender, KeyPressEventArgs e )
        {
            if ( !float.TryParse( textBoxRadius.Text + e.KeyChar, out float a ) && e.KeyChar != 8 )
            {
                e.Handled = true;
            }
        }
    }
}