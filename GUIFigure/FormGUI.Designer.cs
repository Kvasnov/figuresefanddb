﻿namespace GUIFigure
{
    sealed partial class FormGui
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridFigure = new System.Windows.Forms.DataGridView();
            this.buttonCircle = new System.Windows.Forms.Button();
            this.buttonRectangle = new System.Windows.Forms.Button();
            this.buttonTriangle = new System.Windows.Forms.Button();
            this.buttonAddCircle = new System.Windows.Forms.Button();
            this.buttonDeleteCircle = new System.Windows.Forms.Button();
            this.buttonAddRectangle = new System.Windows.Forms.Button();
            this.buttonDeleteRectangle = new System.Windows.Forms.Button();
            this.buttonAddTriangle = new System.Windows.Forms.Button();
            this.buttonDeleteTriangle = new System.Windows.Forms.Button();
            this.fileSystemWatcher1 = new System.IO.FileSystemWatcher();
            this.buttonUpdateCircle = new System.Windows.Forms.Button();
            this.buttonUpdateRectangle = new System.Windows.Forms.Button();
            this.pictureBoxFigures = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridFigure)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fileSystemWatcher1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxFigures)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridFigure
            // 
            this.dataGridFigure.AllowUserToOrderColumns = true;
            this.dataGridFigure.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridFigure.Location = new System.Drawing.Point(12, 117);
            this.dataGridFigure.Name = "dataGridFigure";
            this.dataGridFigure.RowHeadersWidth = 62;
            this.dataGridFigure.Size = new System.Drawing.Size(727, 325);
            this.dataGridFigure.TabIndex = 0;
            // 
            // buttonCircle
            // 
            this.buttonCircle.Location = new System.Drawing.Point(12, 16);
            this.buttonCircle.Name = "buttonCircle";
            this.buttonCircle.Size = new System.Drawing.Size(236, 45);
            this.buttonCircle.TabIndex = 1;
            this.buttonCircle.Text = "Circles";
            this.buttonCircle.UseVisualStyleBackColor = true;
            this.buttonCircle.Click += new System.EventHandler(this.buttonCircle_Click);
            // 
            // buttonRectangle
            // 
            this.buttonRectangle.Location = new System.Drawing.Point(254, 16);
            this.buttonRectangle.Name = "buttonRectangle";
            this.buttonRectangle.Size = new System.Drawing.Size(236, 45);
            this.buttonRectangle.TabIndex = 2;
            this.buttonRectangle.Text = "Rectangles";
            this.buttonRectangle.UseVisualStyleBackColor = true;
            this.buttonRectangle.Click += new System.EventHandler(this.ButtonRectangle_Click);
            // 
            // buttonTriangle
            // 
            this.buttonTriangle.Location = new System.Drawing.Point(516, 16);
            this.buttonTriangle.Name = "buttonTriangle";
            this.buttonTriangle.Size = new System.Drawing.Size(223, 45);
            this.buttonTriangle.TabIndex = 3;
            this.buttonTriangle.Text = "Triangles";
            this.buttonTriangle.UseVisualStyleBackColor = true;
            // 
            // buttonAddCircle
            // 
            this.buttonAddCircle.Location = new System.Drawing.Point(12, 74);
            this.buttonAddCircle.Name = "buttonAddCircle";
            this.buttonAddCircle.Size = new System.Drawing.Size(75, 23);
            this.buttonAddCircle.TabIndex = 4;
            this.buttonAddCircle.Text = "Add";
            this.buttonAddCircle.UseVisualStyleBackColor = true;
            this.buttonAddCircle.Click += new System.EventHandler(this.buttonAddCircle_Click);
            // 
            // buttonDeleteCircle
            // 
            this.buttonDeleteCircle.Location = new System.Drawing.Point(174, 74);
            this.buttonDeleteCircle.Name = "buttonDeleteCircle";
            this.buttonDeleteCircle.Size = new System.Drawing.Size(75, 23);
            this.buttonDeleteCircle.TabIndex = 5;
            this.buttonDeleteCircle.Text = "Delete";
            this.buttonDeleteCircle.UseVisualStyleBackColor = true;
            this.buttonDeleteCircle.Click += new System.EventHandler(this.buttonDeleteCircle_Click);
            // 
            // buttonAddRectangle
            // 
            this.buttonAddRectangle.Location = new System.Drawing.Point(254, 74);
            this.buttonAddRectangle.Name = "buttonAddRectangle";
            this.buttonAddRectangle.Size = new System.Drawing.Size(75, 23);
            this.buttonAddRectangle.TabIndex = 6;
            this.buttonAddRectangle.Text = "Add";
            this.buttonAddRectangle.UseVisualStyleBackColor = true;
            this.buttonAddRectangle.Click += new System.EventHandler(this.ButtonAddRectangle_Click);
            // 
            // buttonDeleteRectangle
            // 
            this.buttonDeleteRectangle.Location = new System.Drawing.Point(415, 74);
            this.buttonDeleteRectangle.Name = "buttonDeleteRectangle";
            this.buttonDeleteRectangle.Size = new System.Drawing.Size(75, 23);
            this.buttonDeleteRectangle.TabIndex = 7;
            this.buttonDeleteRectangle.Text = "Delete";
            this.buttonDeleteRectangle.UseVisualStyleBackColor = true;
            this.buttonDeleteRectangle.Click += new System.EventHandler(this.ButtonDeleteRectangle_Click);
            // 
            // buttonAddTriangle
            // 
            this.buttonAddTriangle.Location = new System.Drawing.Point(516, 74);
            this.buttonAddTriangle.Name = "buttonAddTriangle";
            this.buttonAddTriangle.Size = new System.Drawing.Size(75, 23);
            this.buttonAddTriangle.TabIndex = 8;
            this.buttonAddTriangle.Text = "Add";
            this.buttonAddTriangle.UseVisualStyleBackColor = true;
            // 
            // buttonDeleteTriangle
            // 
            this.buttonDeleteTriangle.Location = new System.Drawing.Point(664, 74);
            this.buttonDeleteTriangle.Name = "buttonDeleteTriangle";
            this.buttonDeleteTriangle.Size = new System.Drawing.Size(75, 23);
            this.buttonDeleteTriangle.TabIndex = 9;
            this.buttonDeleteTriangle.Text = "Delete";
            this.buttonDeleteTriangle.UseVisualStyleBackColor = true;
            // 
            // fileSystemWatcher1
            // 
            this.fileSystemWatcher1.EnableRaisingEvents = true;
            this.fileSystemWatcher1.SynchronizingObject = this;
            // 
            // buttonUpdateCircle
            // 
            this.buttonUpdateCircle.Location = new System.Drawing.Point(93, 74);
            this.buttonUpdateCircle.Name = "buttonUpdateCircle";
            this.buttonUpdateCircle.Size = new System.Drawing.Size(75, 23);
            this.buttonUpdateCircle.TabIndex = 6;
            this.buttonUpdateCircle.Text = "Update";
            this.buttonUpdateCircle.UseVisualStyleBackColor = true;
            this.buttonUpdateCircle.Click += new System.EventHandler(this.buttonUpdateCircle_Click);
            // 
            // buttonUpdateRectangle
            // 
            this.buttonUpdateRectangle.Location = new System.Drawing.Point(334, 74);
            this.buttonUpdateRectangle.Name = "buttonUpdateRectangle";
            this.buttonUpdateRectangle.Size = new System.Drawing.Size(75, 23);
            this.buttonUpdateRectangle.TabIndex = 10;
            this.buttonUpdateRectangle.Text = "Update";
            this.buttonUpdateRectangle.UseVisualStyleBackColor = true;
            this.buttonUpdateRectangle.Click += new System.EventHandler(this.ButtonUpdateRectangle_Click);
            // 
            // pictureBoxFigures
            // 
            this.pictureBoxFigures.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBoxFigures.Location = new System.Drawing.Point(761, 16);
            this.pictureBoxFigures.Name = "pictureBoxFigures";
            this.pictureBoxFigures.Size = new System.Drawing.Size(550, 420);
            this.pictureBoxFigures.TabIndex = 11;
            this.pictureBoxFigures.TabStop = false;
            this.pictureBoxFigures.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pictureBoxFigures_MouseDown);
            this.pictureBoxFigures.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pictureBoxFigures_MouseMove);
            this.pictureBoxFigures.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pictureBoxFigures_MouseUp);
            // 
            // FormGui
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1333, 471);
            this.Controls.Add(this.pictureBoxFigures);
            this.Controls.Add(this.buttonUpdateRectangle);
            this.Controls.Add(this.buttonDeleteTriangle);
            this.Controls.Add(this.buttonAddTriangle);
            this.Controls.Add(this.buttonDeleteRectangle);
            this.Controls.Add(this.buttonUpdateCircle);
            this.Controls.Add(this.buttonAddRectangle);
            this.Controls.Add(this.buttonDeleteCircle);
            this.Controls.Add(this.buttonAddCircle);
            this.Controls.Add(this.buttonTriangle);
            this.Controls.Add(this.buttonRectangle);
            this.Controls.Add(this.buttonCircle);
            this.Controls.Add(this.dataGridFigure);
            this.Name = "FormGui";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridFigure)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fileSystemWatcher1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxFigures)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button buttonCircle;
        private System.Windows.Forms.Button buttonRectangle;
        private System.Windows.Forms.Button buttonTriangle;
        private System.Windows.Forms.Button buttonAddCircle;
        private System.Windows.Forms.Button buttonDeleteCircle;
        private System.Windows.Forms.Button buttonAddRectangle;
        private System.Windows.Forms.Button buttonDeleteRectangle;
        private System.Windows.Forms.Button buttonAddTriangle;
        private System.Windows.Forms.Button buttonDeleteTriangle;
        private System.IO.FileSystemWatcher fileSystemWatcher1;
        private System.Windows.Forms.Button buttonUpdateCircle;
        public System.Windows.Forms.DataGridView dataGridFigure;
        private System.Windows.Forms.Button buttonUpdateRectangle;
        private System.Windows.Forms.PictureBox pictureBoxFigures;
    }
}

