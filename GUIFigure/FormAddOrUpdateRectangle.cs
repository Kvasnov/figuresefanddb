﻿using System.Windows.Forms;
using Models;

namespace GUIFigure
{
    public partial class FormAddOrUpdateRectangle : Form
    {
        public FormAddOrUpdateRectangle( RectangleModel model )
        {
            InitializeComponent();

            _model = model;
            textBoxHeight.DataBindings.Add( new Binding( nameof( TextBox.Text ), _model, nameof( RectangleModel.Height ) ) { DataSourceUpdateMode = DataSourceUpdateMode.OnPropertyChanged } );
            textBoxWidth.DataBindings.Add( new Binding( nameof( TextBox.Text ), _model, nameof( RectangleModel.Width ) ) { DataSourceUpdateMode = DataSourceUpdateMode.OnPropertyChanged } );

            buttonOK.DataBindings.Add( new Binding( nameof( Enabled ), _model, nameof( RectangleModel.IsValid ) ) );
        }

        private readonly RectangleModel _model;

        private void textBoxWidth_KeyPress( object sender, KeyPressEventArgs e )
        {
            if ( !float.TryParse( textBoxWidth.Text + e.KeyChar, out float a ) && e.KeyChar != 8 )
            {
                e.Handled = true;
            }
        }

        private void textBoxHeight_KeyPress( object sender, KeyPressEventArgs e )
        {
            if ( !float.TryParse( textBoxHeight.Text + e.KeyChar, out float a ) && e.KeyChar != 8 )
            {
                e.Handled = true;
            }
        }
    }
}