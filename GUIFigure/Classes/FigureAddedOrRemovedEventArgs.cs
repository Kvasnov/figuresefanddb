﻿using System;

namespace GUIFigure.Classes
{
    public class FigureAddedOrRemovedEventArgs<T>: EventArgs
    {
        public T Figure { get; }

        public FigureAddedOrRemovedEventArgs(T figure)
        {
            Figure = figure;
        }
    }
}
