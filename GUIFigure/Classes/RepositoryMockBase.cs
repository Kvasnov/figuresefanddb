﻿using System;
using System.Collections.Generic;
using System.Linq;
using Figures.DbLibrary.Entities.Abstract;
using GUIFigure.Interfaces;

namespace GUIFigure.Classes
{
    public abstract class RepositoryMockBase< TEntity > : IFigureRepository< TEntity > where TEntity : BaseEntity, new()
    {
        private static readonly List< BaseEntity > Figures = new List< BaseEntity >();

        static RepositoryMockBase()
        {
            //Figures.Add(new Circle(){Radius = 5});
            //Figures.Add(new Rectangle(){Height = 5, Width = 5});
        }

        public event EventHandler< FigureAddedOrRemovedEventArgs< TEntity > > FigureAdded;
        public event EventHandler< FigureAddedOrRemovedEventArgs< TEntity > > FigureRemoved;
        public event EventHandler< FigureAddedOrRemovedEventArgs< TEntity > > FigureUpdated;

        public virtual IEnumerable< TEntity > GetFiguresList()
        {
            return Figures.OfType< TEntity >();
        }

        public virtual TEntity GetFigure( Guid id )
        {
            return Figures.OfType< TEntity >().FirstOrDefault( t => t.Id == id );
        }

        public virtual void Add( TEntity entity )
        {
            Figures.Add( entity );
            OnFigureAdded( new FigureAddedOrRemovedEventArgs< TEntity >( entity ) );
        }

        public virtual void Remove( Guid id )
        {
            Figures.Remove( GetFigure( id ) );
            OnFigureRemove( new FigureAddedOrRemovedEventArgs< TEntity >( GetFigure( id ) ) );
        }

        public void Update( TEntity entity )
        {
            //Figures[indexFigure] = entity;
            OnFigureUpdated( new FigureAddedOrRemovedEventArgs< TEntity >( entity ) );
        }

        public virtual void Dispose()
        {
        }

        protected virtual void OnFigureRemove( FigureAddedOrRemovedEventArgs< TEntity > e )
        {
            FigureRemoved?.Invoke( this, e );
        }

        protected virtual void OnFigureAdded( FigureAddedOrRemovedEventArgs< TEntity > e )
        {
            FigureAdded?.Invoke( this, e );
        }

        protected virtual void OnFigureUpdated( FigureAddedOrRemovedEventArgs< TEntity > e )
        {
            FigureUpdated?.Invoke( this, e );
        }
    }
}