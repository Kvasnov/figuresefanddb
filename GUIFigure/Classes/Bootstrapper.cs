﻿using Autofac;
using Figures.DbLibrary.Entities;
using GUIFigure.Interfaces;
using GUIFigure.RepositoriesForORM;

namespace GUIFigure.Classes
{
    public class Bootstrapper
    {
        public static IContainer Bootstrap()
        {
            var builder = new ContainerBuilder();

            builder.RegisterType<FormGui>().AsSelf();
            builder.RegisterType<CircleRepositoryMock>().As<IFigureRepository<Circle>>();
            builder.RegisterType<RectangleRepositoryMock>().As<IFigureRepository<RectangleFigure>>();

            return builder.Build();
        }
    }
}
