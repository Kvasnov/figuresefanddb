﻿using System.Drawing;
using System.Drawing.Drawing2D;
using Figures.DbLibrary.Entities;

namespace GUIFigure.Classes
{
    public class Renderer
    {
        public Renderer( Graphics graphics )
        {
            _graphics = graphics;
        }

        private readonly Graphics _graphics;
        private readonly Pen _thickRedPen = new Pen( Color.Red, 2 );
        private readonly Pen _thinBlackPen = new Pen( Color.Black, 1 );

        public void Clear()
        {
            _graphics.Clear( Color.AliceBlue );
            _graphics.SmoothingMode = SmoothingMode.HighQuality;
        }

        public void Draw( Circle circle )
        {
            _graphics.DrawEllipse( _thinBlackPen, circle.BeginPoint.X, circle.BeginPoint.Y, circle.Radius * 2f, circle.Radius * 2f );
        }

        public void Draw( RectangleFigure rectangle )
        {
            _graphics.DrawRectangle( _thinBlackPen, rectangle.BeginPoint.X, rectangle.BeginPoint.Y, rectangle.Width * 2f, rectangle.Height * 2f );
        }
    }
}