﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using Figures.DbLibrary.Entities.Abstract;
using Figures.DbLibrary.Infrastructure;
using GUIFigure.Interfaces;

namespace GUIFigure.Classes
{
    public abstract class DbContextRepository<TEntity, TContext> : IFigureRepository<TEntity>
        where TContext : FiguresContext, new()
        where TEntity : BaseEntity
    {
        public event EventHandler<FigureAddedOrRemovedEventArgs<TEntity>> FigureAdded;
        public event EventHandler<FigureAddedOrRemovedEventArgs<TEntity>> FigureRemoved;
        public event EventHandler<FigureAddedOrRemovedEventArgs<TEntity>> FigureUpdated;

        public virtual IEnumerable<TEntity> GetFiguresList()
        {
            using (var context = new TContext())
            {
                context.Set<TEntity>().Load();
                return context.Set<TEntity>().Local.ToBindingList();
            }
        }

        public virtual TEntity GetFigure(Guid id)
        {
            using (var context = new TContext())
            {
                return context.Set<TEntity>().Find(id);
            }
        }

        public virtual void Add(TEntity entity)
        {
            using (var context = new TContext())
            {
                context.Set<TEntity>().Add(entity);
                context.SaveChanges();
                OnFigureAdded(new FigureAddedOrRemovedEventArgs<TEntity>(entity));
            }
        }

        public virtual void Remove(Guid id)
        {
            using (var context = new TContext())
            {
                if (id != Guid.Empty)
                {
                    var entityToDelete = context.Set<TEntity>().Find(id);
                    if (entityToDelete != null)
                    {
                        context.Set<TEntity>().Remove(entityToDelete);
                        context.SaveChanges();
                        OnFigureRemove(new FigureAddedOrRemovedEventArgs<TEntity>(entityToDelete));
                    }
                }
            }
        }

        public void Update(TEntity entity)
        {
            using (var context = new TContext())
            {
                context.Set<TEntity>().Attach(entity);
                context.Entry(entity).State = EntityState.Modified;
                context.SaveChanges();
                OnFigureUpdated(new FigureAddedOrRemovedEventArgs<TEntity>(entity));
            }
        }

        public virtual void Dispose()
        {
        }

        protected virtual void OnFigureRemove(FigureAddedOrRemovedEventArgs<TEntity> e)
        {
            FigureRemoved?.Invoke(this, e);
        }

        protected virtual void OnFigureAdded(FigureAddedOrRemovedEventArgs<TEntity> e)
        {
            FigureAdded?.Invoke(this, e);
        }

        protected virtual void OnFigureUpdated(FigureAddedOrRemovedEventArgs<TEntity> e)
        {
            FigureUpdated?.Invoke(this, e);
        }


    }
}
