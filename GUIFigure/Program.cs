﻿/*

Осознай, что произошло. Затем сделай стабовые реализации IFigureRepository<Circle>, IFigureRepository<Rectangle>, 
которые будут работать не с настоящей базой, а, например, со статическими списками. Попробуй инициализацию DI поменять. 
Опять осознай, какие возможности это даёт.
Затем попробуй отрисовать все эти фигуры, добавив им какие-нибудь ещё поля координат, визуально. 
Реализуй возможность изменения их положения и размера с помощью мышки

*/
using System;
using System.Windows.Forms;
using Autofac;
using GUIFigure.Classes;

namespace GUIFigure
{
    internal static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            var conteiner = Bootstrapper.Bootstrap();

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(conteiner.Resolve<FormGui>());
            // Единственное упоминание CircleRepository,
            // во всех остальных случаях используется ICircleRepository
        }
    }
}
