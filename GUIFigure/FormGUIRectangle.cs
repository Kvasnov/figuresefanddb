﻿using System;
using System.Linq;
using System.Windows.Forms;
using Figures.DbLibrary.Entities;
using GUIFigure.Classes;
using Models;

namespace GUIFigure
{
    public sealed partial class FormGui : Form
    {
        // Прямоугольники тут

        private void _rectangleRepository_RectangleUpdated( object sender, FigureAddedOrRemovedEventArgs< RectangleFigure > e )
        {
            MessageBox.Show( $"Rectangle (Id = {e.Figure.Id}) has been updated" );
        }

        private void _rectangleRepository_RectangleRemoved( object sender, FigureAddedOrRemovedEventArgs< RectangleFigure > e )
        {
            MessageBox.Show( $"Rectangle (Id = {e.Figure.Id}) has been removed" );
        }

        private void _rectangleRepository_RectangleAdded( object sender, FigureAddedOrRemovedEventArgs< RectangleFigure > e )
        {
            MessageBox.Show( $"Rectangle (Id = {e.Figure.Id}) has been added" );
        }

        private void UpdateRectangleUi()
        {
            dataGridFigure.DataSource = _rectangleRepository.GetFiguresList().ToList();
        }

        private void RectangleCollectionChanged( object sender, FigureAddedOrRemovedEventArgs< RectangleFigure > e )
        {
            UpdateRectangleUi();
        }

        private void ButtonRectangle_Click( object sender, EventArgs e )
        {
            UpdateRectangleUi();
        }

        private void ButtonAddRectangle_Click( object sender, EventArgs e )
        {
            var rectangle = new RectangleFigure();
            var model = new RectangleModel( rectangle );
            using ( var addRectangle = new FormAddOrUpdateRectangle( model ) )
            {
                var result = addRectangle.ShowDialog( this );

                if ( result == DialogResult.Cancel )
                    return;
            }

            _rectangleRepository.Add( rectangle );
        }

        private void ButtonUpdateRectangle_Click( object sender, EventArgs e )
        {
            if ( dataGridFigure.CurrentRow == null )
                return;

            var rectangle = ( RectangleFigure ) dataGridFigure.CurrentRow.DataBoundItem;
            var model = new RectangleModel( rectangle );
            using ( var updateRectangle = new FormAddOrUpdateRectangle( model ) )
            {
                var result = updateRectangle.ShowDialog( this );

                if ( result == DialogResult.Cancel )
                    return;
            }

            var indexRectangle = dataGridFigure.CurrentRow.Index;
            _rectangleRepository.Update( rectangle );
        }

        private void ButtonDeleteRectangle_Click( object sender, EventArgs e )
        {
            if ( dataGridFigure.CurrentRow == null )
                return;

            using ( var deleteRectangle = new FormQuestionDelete() )
            {
                var result = deleteRectangle.ShowDialog( this );

                if ( result == DialogResult.Cancel )
                    return;

                var rectangle = ( RectangleFigure ) dataGridFigure.CurrentRow.DataBoundItem;
                _rectangleRepository.Remove( rectangle.Id );
            }
        }
    }
}