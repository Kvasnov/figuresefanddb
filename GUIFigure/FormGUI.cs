﻿/*

Следующим шагом нужно сделать GUI для этого всего. Сначала с помощью DataGrid'а, например. 
Там как раз вчерашние вопросы возникнут про редактирование объекта в GUI

События позволяют тебе разделить код и не думать дважды.
То есть подписываешься на событие какое-то и знаешь, как на это реагировать.
А там, где это событие происходит, ты ничего не делаешь.
Представь себе будильник. Ты его поставил и ждёшь, спишь. Будильник о тебе ничего не знает, но ты проснёшься, когда он зазвенит.

*/

using System;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using Figures.DbLibrary.Entities;
using Figures.DbLibrary.Entities.Abstract;
using GUIFigure.Classes;
using GUIFigure.Interfaces;
using Models;
using Models.Wrappers;

namespace GUIFigure
{
    public sealed partial class FormGui : Form
    {
        public FormGui( IFigureRepository< Circle > circleRepository, IFigureRepository< RectangleFigure > rectangleRepository )
        {
            _circleRepository = circleRepository;
            _rectangleRepository = rectangleRepository;
            InitializeComponent();
            Closed += FormGUI_Closed;
            _circleRepository.FigureAdded += CircleCollectionChanged;
            _circleRepository.FigureRemoved += CircleCollectionChanged;
            _circleRepository.FigureUpdated += CircleCollectionChanged;

            _rectangleRepository.FigureAdded += RectangleCollectionChanged;
            _rectangleRepository.FigureRemoved += RectangleCollectionChanged;
            _rectangleRepository.FigureUpdated += RectangleCollectionChanged;

            _rectangleRepository.FigureAdded += _rectangleRepository_RectangleAdded;
            _rectangleRepository.FigureRemoved += _rectangleRepository_RectangleRemoved;
            _rectangleRepository.FigureUpdated += _rectangleRepository_RectangleUpdated;

            pictureBoxFigures.Paint += PictureBoxFigures_Paint;
        }

        private readonly IFigureRepository< Circle > _circleRepository;
        private readonly IFigureRepository< RectangleFigure > _rectangleRepository;
        private BaseEntity _selectedFigure;

        private void PictureBoxFigures_Paint( object sender, PaintEventArgs e )
        {
            var renderer = new Renderer( e.Graphics );
            renderer.Clear();
            foreach ( var circle in _circleRepository.GetFiguresList() )
            {
                //renderer.Draw(circle);
                var wrap = new CircleWrapper( circle, e.Graphics );
                wrap.Draw( circle );
            }

            foreach ( var rectangle in _rectangleRepository.GetFiguresList() )
            {
                renderer.Draw( rectangle );
            }
        }

        private void UpdateCircleUi()
        {
            //dataGridFigure.DataSource = new BindingList<Circle> ( _circleRepository.GetFiguresList().ToList());
            dataGridFigure.DataSource = _circleRepository.GetFiguresList().ToList();
        }

        private void CircleCollectionChanged( object sender, FigureAddedOrRemovedEventArgs< Circle > e )
        {
            UpdateCircleUi();
            pictureBoxFigures.Invalidate();
        }

        private void FormGUI_Closed( object sender, EventArgs e )
        {
            _circleRepository?.Dispose();
        }

        private void buttonCircle_Click( object sender, EventArgs e )
        {
            UpdateCircleUi();
        }

        private void buttonAddCircle_Click( object sender, EventArgs e )
        {
            var circle = new Circle();
            var model = new CircleModel( circle );
            using ( var addCircle = new FormAddOrUpdateCircle( model ) )
            {
                var result = addCircle.ShowDialog( this );

                if ( result == DialogResult.Cancel )
                    return;
            }

            _circleRepository.Add( circle );
        }

        private void buttonUpdateCircle_Click( object sender, EventArgs e )
        {
            if ( dataGridFigure.CurrentRow == null )
                return;

            var circle = ( Circle ) dataGridFigure.CurrentRow.DataBoundItem;
            var model = new CircleModel( circle );
            using ( var updateCircle = new FormAddOrUpdateCircle( model ) )
            {
                var result = updateCircle.ShowDialog( this );

                if ( result == DialogResult.Cancel )
                    return;
            }

            _circleRepository.Update( circle );
        }

        private void buttonDeleteCircle_Click( object sender, EventArgs e )
        {
            if ( dataGridFigure.CurrentRow == null )
                return;

            using ( var deleteCircle = new FormQuestionDelete() )
            {
                var result = deleteCircle.ShowDialog( this );

                if ( result == DialogResult.Cancel )
                    return;

                var circle = ( Circle ) dataGridFigure.CurrentRow.DataBoundItem;
                _circleRepository.Remove( circle.Id );
            }
        }

        private void pictureBoxFigures_MouseDown( object sender, MouseEventArgs e )
        {
            if ( e.Button == MouseButtons.Left )
            {
                _selectedFigure = _circleRepository.GetFiguresList().
                                                    Select( _ => new { SquaredDistance = Math.Pow( e.X - _.BeginPoint.X - _.Radius, 2d ) + Math.Pow( e.Y - _.BeginPoint.Y - _.Radius, 2d ), Circle = _ } ).
                                                    OrderBy( _ => _.SquaredDistance ).
                                                    Where( _ => _.SquaredDistance <= Math.Pow( _.Circle.Radius, 2d ) ).
                                                    Select( _ => _.Circle ).
                                                    FirstOrDefault();
            }
        }

        private void pictureBoxFigures_MouseUp( object sender, MouseEventArgs e )
        {
            if ( e.Button == MouseButtons.Left )
            {
                _selectedFigure = null;
            }
        }

        private void pictureBoxFigures_MouseMove( object sender, MouseEventArgs e )
        {
            if ( e.Button == MouseButtons.Left )
            {
                if ( _selectedFigure is Circle circle )
                {
                    circle.BeginPoint = new Point( ( int ) ( e.X - circle.Radius ), ( int ) ( e.Y - circle.Radius ) );
                    _circleRepository.Update( circle );
                }
                else if ( _selectedFigure is RectangleFigure rectangle )
                {
                    rectangle.BeginPoint = new Point( ( int ) ( e.X - rectangle.Width ), ( int ) ( e.Y - rectangle.Height ) );
                    _rectangleRepository.Update( rectangle );
                }
            }
        }

        //пока что работаю только с кругами и прямоугольниками

        //private void buttonTriangle_Click(object sender, EventArgs e)
        //{
        //    using (var context = new FiguresContext())
        //    {
        //        context.Triangles.Load();
        //        dataGridFigure.DataSource = context.Triangles.Local.ToBindingList();
        //    }
        //}
    }
}