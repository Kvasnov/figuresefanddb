﻿using System.Drawing;
using Figures.DbLibrary.Entities;
using GUIFigure.Interfaces;

namespace Models.Wrappers
{
    internal sealed class CircleWrapper : CircleModel, IDrawble< Circle >
    {
        public CircleWrapper( Circle model, Graphics graphics ) : base( model )
        {
            _graphics = graphics;
        }

        private readonly Graphics _graphics;
        private readonly Pen _thickRedPen = new Pen( Color.Red, 2 );
        private readonly Pen _thinBlackPen = new Pen( Color.Black, 1 );

        public void Draw( Circle circle )
        {
            _graphics.DrawEllipse( _thinBlackPen, circle.BeginPoint.X, circle.BeginPoint.Y, circle.Radius * 2f, circle.Radius * 2f );
        }
    }
}