﻿using Figures.DbLibrary.Entities;

namespace Models.Wrappers
{
    public class RectangleWrapper : RectangleModel
    {
        public RectangleWrapper( RectangleFigure model ) : base( model )
        {
        }
    }
}