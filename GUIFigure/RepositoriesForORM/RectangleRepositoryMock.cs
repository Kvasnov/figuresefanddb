﻿using Figures.DbLibrary.Entities;
using GUIFigure.Classes;

namespace GUIFigure.RepositoriesForORM
{
    public class RectangleRepositoryMock : RepositoryMockBase<RectangleFigure>
    {

        public RectangleRepositoryMock()
        {
            for (var i = 0; i < 50; i++)
            {
                Add(new RectangleFigure
                {
                    Height = i + 20,
                    Width = i + 30
                });
            }
        }
    }
}
