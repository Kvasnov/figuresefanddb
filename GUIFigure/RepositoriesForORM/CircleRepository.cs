﻿/*

- Сможем как нибудь поработать с делегатами Action и Func?
- Для практики давай сделаем массив функций, которые будут делать некоторые операции над числами.

*/

using Figures.DbLibrary.Entities;
using GUIFigure.Classes;
using Figures.DbLibrary.Infrastructure;
using GUIFigure.Interfaces;

namespace GUIFigure.RepositoriesForORM
{
    public class CircleRepository : DbContextRepository<Circle, FiguresContext>, IFigureRepository<Circle>
    {
    }
}

