﻿using Figures.DbLibrary.Entities;
using GUIFigure.Classes;

namespace GUIFigure.RepositoriesForORM
{
    public class CircleRepositoryMock : RepositoryMockBase<Circle>
    {
        public CircleRepositoryMock()
        {
            for (var i = 0; i < 30; i++)
            {
                Add(new Circle
                {
                    Radius = i + 10
                });
            }
        }
    }
}
