﻿using Figures.DbLibrary.Entities;

namespace Models
{
    public class RectangleModel : BaseEntityModel< RectangleFigure >
    {
        public RectangleModel( RectangleFigure model ) : base( model )
        {
        }

        public float Width
        {
            get => _model.Width;
            set
            {
                if ( _model.Width == value )
                    return;
                _model.Width = value;
                OnPropertyChanged();
            }
        }

        public float Height
        {
            get => _model.Height;
            set
            {
                if ( _model.Height == value )
                    return;
                _model.Height = value;
                OnPropertyChanged();
            }
        }

        public override bool IsValid => Width > 0d && Height > 0d;
    }
}