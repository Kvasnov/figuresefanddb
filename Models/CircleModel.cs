﻿using Figures.DbLibrary.Entities;

namespace Models
{
    public class CircleModel: BaseEntityModel<Circle>
    {
        public CircleModel(Circle model) : base(model)
        {
        }

        public float Radius
        {
            get => _model.Radius;
            set
            {
                if (_model.Radius == value) return;
                _model.Radius = value;
                OnPropertyChanged();
            }
        }
        public override bool IsValid => Radius > 0f;

    }
}
