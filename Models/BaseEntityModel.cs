﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using Figures.DbLibrary.Entities.Abstract;

namespace Models
{
    public abstract class BaseEntityModel<T> : INotifyPropertyChanged
        where T : BaseEntity
    {

        protected BaseEntityModel(T model)
        {
            _model = model;
        }
        public abstract bool IsValid { get; }

        public Guid Id
        {
            get => _model.Id;
            set
            {
                if (_model.Id == value) return;
                _model.Id = value;
                OnPropertyChanged();
            }
        }

        protected readonly T _model;

        protected T Model => _model;

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

    }
}
