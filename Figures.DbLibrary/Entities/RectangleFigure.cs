﻿using Figures.DbLibrary.Entities.Abstract;

namespace Figures.DbLibrary.Entities
{
    public class RectangleFigure : BaseEntity
    {
        public float Width { get; set; }
        public float Height { get; set; }
    }

}
