﻿using Figures.DbLibrary.Entities.Abstract;

namespace Figures.DbLibrary
{
    public class Rectangle : BaseEntity
    {
        public float Width { get; set; }
        public float Height { get; set; }
    }

}
