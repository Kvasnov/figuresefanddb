﻿using Figures.DbLibrary.Entities.Abstract;

namespace Figures.DbLibrary.Entities
{
    public class Circle : BaseEntity
    {
        public float Radius { get; set; }
    }
}
