﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Drawing;


namespace Figures.DbLibrary.Entities.Abstract
{
    public class BaseEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }

        public Point BeginPoint { get; set; }
        public BaseEntity()
        {
            Id = Guid.NewGuid();

            BeginPoint = new Point(Random.Next(0, 550), Random.Next(0, 420));
        }

        [NotMapped]
        private static readonly Random Random = new Random();

    }
}
