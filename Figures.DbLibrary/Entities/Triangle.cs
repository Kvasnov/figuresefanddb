﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Figures.DbLibrary.Entities
{
    //public class Triangle : BaseEntity
    public class Triangle
    {
        //public string Name { get; set; }
        [Column(Order = 1)]
        public double Cathetus1 { get; set; }
        [Column(Order = 2)]
        public double Cathetus2 { get; set; }
        [Column(Order = 3)]
        public double Hypotenuse { get; set; }

        //public override bool IsValid => throw new System.NotImplementedException();


        public double FigureArea()
        {
            return Cathetus1 + Cathetus2 + Hypotenuse;
        }

        public double Perimeter()
        {
            return 1.0 / 2.0 * Cathetus1 * Cathetus2;
        }
    }
}
