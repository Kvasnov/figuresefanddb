﻿using System.Data.Entity;
using Figures.DbLibrary.Entities;

namespace Figures.DbLibrary.Infrastructure
{
    public class FiguresContext : DbContext
    {
        public FiguresContext() : base( "name = FiguresDBConnectionString" )
        {
        }

        public DbSet< Circle > Circles { get; set; }

        //public DbSet<Triangle> Triangles { get; set; }
        public DbSet< RectangleFigure > Rectangles { get; set; }
    }
}