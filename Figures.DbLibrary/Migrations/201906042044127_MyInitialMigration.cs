using System.Data.Entity.Migrations;

namespace Figures.DbLibrary.Migrations
{
    public partial class MyInitialMigration : DbMigration
    {
        public override void Up()
        {
            CreateTable( "dbo.Circles", c => new { Id = c.Guid( nullable: false, identity: true ), Name = c.String(), Radius = c.Double( nullable: false ), Created = c.DateTime( nullable: false ) } ).PrimaryKey( t => t.Id );

            CreateTable( "dbo.Rectangles", c => new
                {
                    Id = c.Guid( nullable: false, identity: true ),
                    Name = c.String(),
                    Width = c.Double( nullable: false ),
                    Height = c.Double( nullable: false ),
                    Created = c.DateTime( nullable: false )
                } ).
                PrimaryKey( t => t.Id );

            CreateTable( "dbo.Triangles", c => new
                {
                    Id = c.Guid( nullable: false, identity: true ),
                    Name = c.String(),
                    Cathetus1 = c.Double( nullable: false ),
                    Cathetus2 = c.Double( nullable: false ),
                    Hypotenuse = c.Double( nullable: false ),
                    Created = c.DateTime( nullable: false )
                } ).
                PrimaryKey( t => t.Id );
        }

        public override void Down()
        {
            DropTable( "dbo.Triangles" );
            DropTable( "dbo.Rectangles" );
            DropTable( "dbo.Circles" );
        }
    }
}