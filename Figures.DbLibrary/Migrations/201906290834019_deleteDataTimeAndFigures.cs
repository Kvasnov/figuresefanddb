using System.Data.Entity.Migrations;

namespace Figures.DbLibrary.Migrations
{
    public partial class deleteDataTimeAndFigures : DbMigration
    {
        public override void Up()
        {
            DropColumn( "dbo.Circles", "Created" );
            DropTable( "dbo.Rectangles" );
            DropTable( "dbo.Triangles" );
        }

        public override void Down()
        {
            CreateTable( "dbo.Triangles", c => new
                {
                    Cathetus1 = c.Double( nullable: false ),
                    Cathetus2 = c.Double( nullable: false ),
                    Hypotenuse = c.Double( nullable: false ),
                    Id = c.Guid( nullable: false, identity: true ),
                    Created = c.DateTime( nullable: false )
                } ).
                PrimaryKey( t => t.Id );

            CreateTable( "dbo.Rectangles", c => new { Width = c.Double( nullable: false ), Height = c.Double( nullable: false ), Id = c.Guid( nullable: false, identity: true ), Created = c.DateTime( nullable: false ) } ).
                PrimaryKey( t => t.Id );

            AddColumn( "dbo.Circles", "Created", c => c.DateTime( nullable: false ) );
        }
    }
}