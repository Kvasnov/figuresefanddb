using System.Data.Entity.Migrations;

namespace Figures.DbLibrary.Migrations
{
    public partial class AddRectangle : DbMigration
    {
        public override void Up()
        {
            CreateTable( "dbo.Rectangles", c => new { Width = c.Double( nullable: false ), Height = c.Double( nullable: false ), Id = c.Guid( nullable: false, identity: true ) } ).PrimaryKey( t => t.Id );
        }

        public override void Down()
        {
            DropTable( "dbo.Rectangles" );
        }
    }
}