using System.Data.Entity.Migrations;
using Figures.DbLibrary.Infrastructure;

namespace Figures.DbLibrary.Migrations
{
    internal sealed class Configuration : DbMigrationsConfiguration< FiguresContext >
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed( FiguresContext context )
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method
            //  to avoid creating duplicate seed data.
        }
    }
}