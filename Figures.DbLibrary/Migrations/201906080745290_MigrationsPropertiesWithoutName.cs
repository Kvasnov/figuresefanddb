using System.Data.Entity.Migrations;

namespace Figures.DbLibrary.Migrations
{
    public partial class MigrationsPropertiesWithoutName : DbMigration
    {
        public override void Up()
        {
            DropColumn( "dbo.Circles", "Name" );
            DropColumn( "dbo.Rectangles", "Name" );
            DropColumn( "dbo.Triangles", "Name" );
        }

        public override void Down()
        {
            AddColumn( "dbo.Triangles", "Name", c => c.String() );
            AddColumn( "dbo.Rectangles", "Name", c => c.String() );
            AddColumn( "dbo.Circles", "Name", c => c.String() );
        }
    }
}