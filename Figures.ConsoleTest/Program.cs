﻿/*

Изучаем EF.
Солюшн с двумя проектами: консольное для тестов и библиотека с сущностями и работой с базой.
Сделай несколько сущностей (например, те же геометрические фигуры). Сохрани в базу, достань из базы, разные LINQ-запросы попробуй.

*/

using System;
using Figures.DbLibrary.Infrastructure;

namespace Figures.ConsoleTest
{
    public class Program
    {
        private static void Main()
        {
            using ( var context = new FiguresContext() )
            {
                var random = new Random();
                ////добавление элементов
                //for (int i = 0; i < 10; ++i)
                //{
                // context.Circles.Add(new Circle() { Radius = random.NextDouble() * 10 });
                //}

                //for (int i = 0; i < 10; ++i)
                //{
                //    context.Rectangles.Add(new Rectangle()
                //    {
                //        Height = random.NextDouble() * 4,
                //        Width = random.NextDouble() * 8
                //    });
                //}

                //for (int i = 0; i < 10; ++i)
                //{
                //    context.Triangles.Add(new Triangle()
                //    {
                //        Cathetus1 = random.NextDouble() * 3,
                //        Cathetus2 = random.NextDouble() * 3,
                //        Hypotenuse = random.NextDouble() * 5
                //    });
                //}

                //context.SaveChanges();

                // удаление строки
                //for (int i = 0; i < 19; ++i)
                //{
                //context.Circles.Remove(context.Circles.Where(t => t.Radius == 9.4207249439417957).FirstOrDefault());
                //context.SaveChanges();

                //}

                //context.Database.Log += Console.WriteLine;

                //var circleFromSmallToBig = context.Circles.OrderBy(t => t.Radius);

                //context.Database.Log -= Console.WriteLine;

                //var bigRectangle = context.Rectangles.Where(t => t.Height > 2 && t.Width > 4);
                //var takeTriangle = context.Triangles.Take(5);
                ////var skipCircle = context.Circles.OrderBy(t => t.Radius).Skip(5);

                //Console.WriteLine("<<<Circles>>> \n ");
                //foreach (var circle in context.Circles)
                //{
                //    Console.WriteLine($"{circle.Radius}");
                //}

                //Console.WriteLine("\n <<<Rectangles>>> \n ");
                //foreach (var rectangle in bigRectangle)
                //{
                //    Console.WriteLine($"{rectangle.Height}, {rectangle.Width}");
                //}

                //Console.WriteLine("\n <<<Triangles>>> \n");

                //foreach (var triangle in takeTriangle)
                //{
                //    Console.WriteLine($"{triangle.Cathetus1}, {triangle.Cathetus2}, {triangle.Hypotenuse}");
                //}

                //Console.ReadKey();
            }
        }
    }
}